
#[macro_use]
extern crate fstrings;

use std::f32;

fn main() {
    const MAX_POINTS: u32 = 100_000;
    println_f!("MAX_POINTS = {MAX_POINTS}");
    let mut x = 5;
    println_f!("x = {x}");
    x = 6;
    println_f!("x = {x}");

    let z: u32 = "42".parse().expect("Not a number!");
    println_f!("z = {z}");

    const PI: f32 = 3.14159265;
    println_f!("PI = {PI}");

    let lc = 'z';
    let lz = 'ℤ';
    let heart = '😻';
    println!("{} {} {}", lc, lz, heart);

    let tuple: (i32, f64, u8) = (500, 6.4, 1);
    let (a, b, c) = tuple;
    println!("{:?}", tuple);
    println!("{} {} {}", a, b, c);
    println!("{} {} {}", tuple.0, tuple.1, tuple.2);

    let array = [1, 2, 3, 4, 5]; //provide elements of array
    println!("{:?}", array);
    println!("{} {}", array[0], array[2]);

    let array2 = [3; 5]; //provide an element to copy and the number of copies
    println!("{:?}", array2); 

    let months = ["January", "February", "March", "April", "May", "June", "July",
        "August", "September", "October", "November", "December"];
    println!("{:?}", months);
    println!("{}", months[3]);

    another_function(15);

    // expressions
    let f2 = {
        let x = 3;
        x + 1  // like 'return' for expression
    }; // expression that is immediately calculated
    println!("result = {}", f2);



    println!("square of {} = {}", 5, square(5));
    println!("square of {} = {}", 6, square(6));



    print_roots(1.0, 5.0, 6.0);
    print_roots(1.0, 5.0, 10.0);




    let condition = true;
    let number = if condition { 5 } else { 6 };
    println!("The value of number is {}", number);


    let a = [10, 20, 30, 40, 50];
    for element in a.iter() {
        println!("the value is: {}", element);
    }
    for number in (1..4).rev() {
        println!("{}!", number);
    }
}


fn another_function(x: i32) {
    println!("Another function.");
    println!("x = {}", x);
}

fn square(x: i32) -> i32 {
    x * x   // there is no return here!
}

fn find_roots(a: f32, b: f32, c: f32) -> Option<(f32, f32)> {
    let d = b*b - 4.0*a*c;
    if d>0.0 {
        let x1 = (-b + d.sqrt())/(2.0*a);
        let x2 = (-b - d.sqrt())/(2.0*a);
        return Some((x1, x2));
    } else {
        return None;
    }
}

fn print_roots(a: f32, b: f32, c: f32) {
    match find_roots(a, b, c) {
        None => println!("There are no roots"),
        Some((x1, x2)) => {
            println!("The roots are {}, {}", x1, x2)
        }
    }
}
