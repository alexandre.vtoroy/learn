fn main() {
    println!("Examples for Ownership In Rust");

    let mut s1 = String::from("hello"); // here the variable is created, so the memory is allocated
    s1.push_str(" world!");
    println!("{}", s1);

    let s2 = s1; // when we copy the string, we copy only the pointer, not the internal data
                 // Rust will never automatically create “deep” copies of your data
    println!("{}", s2);

    let s3 = s2.clone(); // here we re-create the internal data copying it
    println!("{}", s3);

    // at the end of block, end of visibility scope the variable releases the memory
    // since 's1' is the owner of the string data, the memory is freed


    let x = 5;
    let y = x;     // types such as integers that have a known size at compile time are stored
                   // entirely on the stack, so copies of the actual values are quick to make

    println!("{} {}", x, y);

    // Rust has a special annotation called the Copy trait that we can place on types.
    // If a type implements the Copy trait, an older variable is still usable after assignment.
    // Passing a variable to a function will move or copy, just as assignment does.
    // Returning values can also transfer ownership.
    // A function can take a reference to an object as a parameter instead of taking ownership of the value
    // We call having references as function parameters borrowing.

    let text1 = String::from("hello"); // s comes into scope
    takes_ownership(text1);
    //println!("{}", text1); // compilation error will be generated here

    let text2 = String::from("hello"); // s comes into scope
    not_take_ownership(&text2);
    println!("{}", text2);



    // References can be mutable
    let mut text3 = String::from("hello");
    change(&mut text3);
    println!("{}", text3);



    // Slices:
    let msg = String::from("hello world");
    let hello = &msg[0..5];
    let world = &msg[6..11];
    let slice = &msg[3..];
    println!("{} {} {}", hello, world, slice);

    let a = [1, 2, 3, 4, 5];
    let slice = &a[1..3];
    println!("{:?}", slice);
}

fn takes_ownership(some_string: String) { // some_string comes into scope
    println!("{}", some_string);
}

fn not_take_ownership(some_string: &String) {
    println!("{}", some_string);
}

fn change(some_string: &mut String) {
    some_string.push_str("another text");
}
