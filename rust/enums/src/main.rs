
#[derive(Debug)]
enum IpAddrKind {
    V4,
    V6,
}

#[derive(Debug)]
struct IpAddr {
    kind: IpAddrKind,
    address: String,
}

#[derive(Debug)]
enum IpAddr2 {
    V4(String),
    V6(String),
}

#[derive(Debug)]
enum IpAddr3 {
    V4(u8, u8, u8, u8),
    V6(String),
}

#[derive(Debug)]
enum Message {
    //Quit,
    //Move { x: i32, y: i32 },
    Write(String),
    //ChangeColor(i32, i32, i32),
}

impl Message {
    fn call(&self) {
        println!("{:?}", &self);
    }
}

fn route(ip_kind: IpAddrKind) {
    println!("{:?}", ip_kind);
}

#[derive(Debug)]
enum UsState {
    Alabama,
    Alaska,
    // --snip--
}

#[derive(Debug)]
enum Coin {
    Penny,
    Nickel,
    Dime,
    Quarter(UsState),
}

fn value_in_cents(coin: Coin) -> u8 {
    match coin {
        Coin::Penny => 1,
        Coin::Nickel => 5,
        Coin::Dime => 10,
        Coin::Quarter(state) => {
            println!("State quarter from {:?}!", state);
            25   // equivalent of `return 25;` attention: without ;
        }
    }
}

fn plus_one(x: Option<i32>) -> Option<i32> {
    match x {
        None => None,
        Some(i) => Some(i + 1),
    }
}

fn main() {

    println!("Samples of enums");

    let four = IpAddrKind::V4;
    let six = IpAddrKind::V6;

    route(four);
    route(six);

    let home = IpAddr {
        kind: IpAddrKind::V4,
        address: String::from("127.0.0.1"),
    };

    let loopback = IpAddr {
        kind: IpAddrKind::V6,
        address: String::from("::1"),
    };

    println!("{:?} {:?}", home, loopback);

    let home2 = IpAddr2::V4(String::from("127.0.0.1"));
    let loopback2 = IpAddr2::V6(String::from("::1"));
    println!("{:?} {:?}", home2, loopback2);

    let home3 = IpAddr3::V4(127, 0, 0, 1);
    let loopback3 = IpAddr3::V6(String::from("::1"));
    println!("{:?} {:?}", home3, loopback3);


    let msg = Message::Write(String::from("hello"));
    msg.call();


    let some_number = Some(5);
    let some_string = Some("a string");
    let absent_number: Option<i32> = None;
    println!("{:?} {:?} {:?}", some_number, some_string, absent_number);


    let x: i8 = 5;
    let y: Option<i8> = Some(6);
    
    match y {
        Some(value) => {
            let sum = x + value;
            println!("Sum = {}", sum);
        },
        None => println!("None is passed"),
    }
    

    println!("Value = {}", value_in_cents(Coin::Nickel));
    println!("Value = {}", value_in_cents(Coin::Quarter(UsState::Alabama)));


    println!("Value = {:?}", plus_one(Some(5)));
    println!("Value = {:?}", plus_one(None));



    let some_u8_value = 0u8;
    match some_u8_value {
        1 => println!("one"),
        3 => println!("three"),
        5 => println!("five"),
        7 => println!("seven"),
        _ => (), // the pattern will match any value
    }

    // concise Control Flow with if let
    let some_value = Some(3);
    if let Some(3) = some_value {
        println!("three");
    }

    let str = "abcabc";
    if let str = "*ca*" {
        println!("pattern is matched");
    }
    if str == "*ca*" {
        println!("exact equality");
    }


    let coin = Coin::Quarter(UsState::Alabama);
    let mut count = 0;
    if let Coin::Quarter(state) = coin {
        println!("Found a state quarter from {:?}!", state);
    } else {
        count += 1;
    }
}
