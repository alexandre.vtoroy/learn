
program cond

    implicit none ! no implicit type checking (?)

    ! conditions "if" ".and." ".or."

    real :: a = 1, b = 5, c = 10
    real :: d, x1, x2
    complex :: i = (0, 1)
    integer :: year, month, days
    integer :: score

    d = b**2 - 4*a*c

    if (d>=0) then
        x1 = (-b + sqrt(d))/(2*a)
        x2 = (-b - sqrt(d))/(2*a)
        print *, "two roots:", x1, x2
    else
        print *, "no real roots"
        x1 = -b/(2*a)
        x2 = sqrt(-d)/(2*a)
        print *, x1 + x2*i
        print *, x1 - x2*i
    end if

    ! if (x1<0 .or. x2<0) print *, "negative root"


    year = 2021
    month = 12
   
    select case (month)
        case (1,3,5,7,8,10,12)
            days = 31
        case (2)
            if (mod(year,4)==0) then
                days = 29
            else
                days = 28
            end if
        case (4,6,9,11)
            days = 30
        case default
            print *, "Error, month number not valid."
    end select

    print *, year, month, days



    score = 85
    select case (score)
    case(90:)  ! >=90
        write (*,*) "Grade is: A"
    case(80:89)
        write (*,*) "Grade is: B"
    case(70:79)
        write (*,*) "Grade is: C"
    case(60:69)
        write (*,*) "Grade is: D"
    case(:59)
        write (*,*) "Grade is: F"
    end select
end
