
program loops

    implicit none ! no implicit type checking (?)

    real, parameter :: pi = 3.14159265

    write (*, "(f10.1)") pi
    write (*, "(f10.5)") pi

    write (*, "(a,2x,a,t20,a)") "X", "Y", "Z"

end
