
program loops

    implicit none ! no implicit type checking (?)

    integer :: i, n, s, sum, sumSquares
    real :: value

    s = 0
    do i = 1, 10
        s = s + i
    end do
    print *, s

    s = 0
    do i = 1, 10, 2
        s = s + i
    end do
    print *, s

    do i = 1, 10
        write (*,*) i
        if (i == 5) exit ! exit from loop
    end do

    print *, ""

    do i = 1, 10
        if (i == 5) cycle ! jump to the next iteration of loop
        write (*,*) i
    end do


    n = 100
    sumSquares = 0
    sum = 0
    do i = 1, n+1
        sum = sum + i
        sumSquares = sumSquares + i**2
    end do
    print *, "delta = ", sum**2 - sumSquares


    value = 0.01
    do while (value <= 4.5)
        write (*,*) "Value = ", value
        value = value * 2
    end do
end
