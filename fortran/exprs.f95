
program vars

    implicit none ! no implicit type checking (?)

    integer :: a = 10, b = 20    ! upper/lower case vars are equivalent
    integer :: x, y, z
    real :: ra = 10, rb = 20
    real :: rx
    real, parameter :: pi = 3.14159265
    real :: rad, area


    x = a + b
    y = a - b
    z = a * b
    print *, x, y, z

    rx = ra / rb
    print *, rx

    rx = real(a) / real(b)
    print *, rx, int(rx), nint(rx)    ! conversion to int with truncate and with rounding

    ! power
    print *, a**3


    ! intrinsics
    print *, sin(pi/3), cos(pi/3)

    rad = 1.5
    area = pi * rad**2
    print *, "Area = ", area
end
