
program vars

    implicit none ! no implicit type checking (?)

    integer :: a = 10, b = 20, c = 30    ! upper/lower case vars are equivalent
    real :: x = 1.23
    real, parameter :: pi = 3.14159265   ! declaration of constant
    complex :: z = (1, 2)                ! complex numbers
    logical :: answer = .true.
    character :: letter = 'h'            ! single character
    character (len=80):: msg = "hello, ""world""!"  ! string, double-quotes for quote as character

    integer :: year
    real :: some, count

    ! extended size variables
    ! 64-bits integers
    integer*8 :: bignum = 1
    integer(kind=8) :: bignum2 = 2

    ! 64-bits floats
    real*8 :: rnum = 2.75E5    ! e-notation
    real(kind=8) :: rnum2 = 3.3333E-1


    ! This is a comment line; it is ignored by the compiler
    print *, a, b, c, x, pi, z, answer, letter, msg

    year = 2021
    count = 20
    some = 174.5 * year &   ! continuation line
        + count / 100.0

    print *, some

    print *, bignum, bignum2
    print *, rnum, rnum2

end
